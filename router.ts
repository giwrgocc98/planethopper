import {
  createRouter,
  createWebHashHistory,
  createWebHistory,
  Router,
  RouteRecordRaw,
} from "vue-router";
import Home from "/src/components/Home.vue";

export function initRouter(): Router {
  const routes: RouteRecordRaw[] = [
    {
      name: "Home",
      component: Home,
      path: "/",
    },
  ];

  return createRouter({
    history: createWebHashHistory(),
    routes,
  });
}
