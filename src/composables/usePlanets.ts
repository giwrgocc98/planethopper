import { Planet } from "../types/types";
import { PlanetService } from "../services/PlanetService";
import { Ref, UnwrapRef } from "vue";
export interface UsePlanets {
  getPlanets: (planetService: PlanetService, page: number) => Promise<Planet[]>;
}

export function usePlanets(): UsePlanets {
  async function getPlanets(
    planetService: PlanetService,
    page: number
  ): Promise<Planet[]> {
    return await planetService.getPlanets(page);
  }

  return { getPlanets };
}
