import { createApp } from "vue";
import App from "./App.vue";
import { initRouter } from "../router";

createApp(App).mount("#app");

const router = initRouter();
