import { getImages, getPlanets } from "../client/client";
import { Planet } from "../types/types";
import { Ref, ref, UnwrapRef } from "vue";

export class PlanetService {
  async getPlanets(page: number): Promise<Planet[]> {
    const data: any = await getPlanets(page);
    const images = getImages();
    try {
      return data.results.map(function (obj: any) {
        let temp = new Planet();
        temp.image = images[getRandomInt(3)];
        temp.name = obj.name;
        temp.terrain = obj.terrain;
        temp.population = obj.population;
        return temp;
      });
    } catch {
      console.log("Pages exceeded");
      return [];
    }
  }
}

function getRandomInt(max: number) {
  return Math.floor(Math.random() * max);
}
