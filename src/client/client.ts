export async function getPlanets(page: number) {
  return await fetch("https://swapi.dev/api/planets/?page=" + page)
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      return data;
    });
}

export function getImages() {
  const prefix = ["Skiathos", "Santorini", "Naxos", "Ios"];
  const url = "https://images.ferryhopper.com/locations/";
  return prefix.map((image, index) => url + prefix[index] + ".jpg");
}
