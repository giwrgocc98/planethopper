export type PlanetType = {
  image: string;
  name: string;
  terrain: string;
  population: string;
};

export class Planet implements PlanetType {
  image: string;
  name: string;
  terrain: string;
  population: string;

  constructor(m?: Partial<PlanetType>) {
    this.image = m?.image || "";
    this.name = m?.name || "";
    this.terrain = m?.terrain || "";
    this.population = m?.population || "";
  }
}
