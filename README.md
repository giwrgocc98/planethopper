# Georgios Georgopoulos - Planet Hopper submission
### How to run
- yarn install
- yarn dev

## Technologies used
- Vite 
- Typescript 
- Scss

## Few Words 
Even though it was recommended to use 2.7x version of Vue i decided to go ahead with my preffered Vue 3 script setup version.
I focused more on displaying my skills on typescript and project architecture rather than in UI designing, that's why the page will look really ugly. User's interface was developed in less than an hour. The functionalities are all there though. 


